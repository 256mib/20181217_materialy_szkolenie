
set.seed(20181217)

n_points <- 1000

# wygenerowanie losowych sklepów
lats <- runif(n_points, min = 49.5, max = 54)
lons <- runif(n_points, min = 15, max = 23)
ids <- sprintf('Z%04d', sample(1:5000, n_points))

oddzialy <- data.frame(oddzial = ids, lat = lats, lon = lons)
oddzialy$segment <- sample(1:5, n_points, replace = TRUE)

write.csv2(oddzialy, 'dane/oddzialy.csv', row.names = F, na = '')
