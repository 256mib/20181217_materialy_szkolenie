
library(xml2)

tmp <- read_html('prezentacja_v2.html')

links <- xml_find_all(tmp, './/link')
xml_attr(links[3], 'href') <- 'rtd_style.css'
xml_attr(links[3], 'type') <- 'text/css'

write_html(tmp, 'prezentacja_v3.html')
